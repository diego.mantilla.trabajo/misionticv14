package com.example.misionticv14;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.misionticv14.adapters.AdapterLista;
import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Usuario;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListaAdaptadorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_adaptador);

        ListView lvLista = (ListView) findViewById(R.id.adapter_lista);
        UsuarioADO db = new UsuarioADO(this);
        ArrayList<Usuario> usuarios = db.listar();

        AdapterLista adaptador = new AdapterLista(usuarios);
        lvLista.setAdapter(adaptador);
        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new Mensajes(view.getContext()).alert("Titulo", "Seleccion");
            }
        });


    }
}