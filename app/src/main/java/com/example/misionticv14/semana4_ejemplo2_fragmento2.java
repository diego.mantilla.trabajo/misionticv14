package com.example.misionticv14;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.misionticv14.modelos.Usuario;
import com.example.misionticv14.viewmodels.UsuarioViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento2 extends Fragment {

    public semana4_ejemplo2_fragmento2() {
        // Required empty public constructor
    }

    public static semana4_ejemplo2_fragmento2 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento2 fragment = new semana4_ejemplo2_fragmento2();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View vista = inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento2, container, false);

        EditText txtNombres = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtNombres);
        EditText txtApellidos = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtApellidos);
        EditText txtEmail = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtEmail);
        EditText txtClave = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtClave);

        UsuarioViewModel usuariovm = ViewModelProviders.of(getActivity()).get(UsuarioViewModel.class);
        usuariovm.getUsuario().observe(getViewLifecycleOwner(), new Observer<Usuario>() {
            @Override
            public void onChanged(Usuario usuario) {
                txtNombres.setText(usuario.getNombres());
                txtApellidos.setText(usuario.getApellidos());
                txtEmail.setText(usuario.getEmail());
                txtClave.setText(usuario.getClave());
            }
        });

        return vista;
    }
}