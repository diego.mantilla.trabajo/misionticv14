package com.example.misionticv14.viewmodels;

import com.example.misionticv14.modelos.Usuario;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UsuarioViewModel extends ViewModel {

    private MutableLiveData<Usuario> usuario = new MutableLiveData<>();

    public MutableLiveData<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario.setValue(usuario);
    }
}
