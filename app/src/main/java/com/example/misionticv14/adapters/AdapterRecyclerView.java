package com.example.misionticv14.adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.misionticv14.AdminUsuariosActivity;
import com.example.misionticv14.R;
import com.example.misionticv14.UsuariosInsertarActivity;
import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Usuario;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolderRegistro> {

    private final ArrayList<Usuario> datos;

    public AdapterRecyclerView(ArrayList<Usuario> datos) {
        this.datos = datos;
    }

    @NonNull
    @Override
    public AdapterRecyclerView.ViewHolderRegistro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, null, false);
        return new ViewHolderRegistro(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerView.ViewHolderRegistro holder, int position) {
        holder.asignarRegistros(datos.get(position));
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public class ViewHolderRegistro extends RecyclerView.ViewHolder {

        private TextView txtNombreCompleto;
        private TextView txtEmail;
        private String id;

        public ViewHolderRegistro(@NonNull View itemView) {
            super(itemView);

            txtNombreCompleto = (TextView) itemView.findViewById(R.id.recyclerviewitem_txtNombreCompleto);
            txtEmail = (TextView) itemView.findViewById(R.id.recyclerviewitem_txtEmail);
            ImageButton btnEditar = (ImageButton) itemView.findViewById(R.id.recyclerviewitem_btnEditar);
            ImageButton btnEliminar = (ImageButton) itemView.findViewById(R.id.recyclerviewitem_btnEliminar);

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(view.getContext(), UsuariosInsertarActivity.class);
                    i.putExtra("id", id);
                    view.getContext().startActivity(i);
                }
            });

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder msj = new AlertDialog.Builder(view.getContext());
                    msj.setTitle("Confirmación");
                    msj.setMessage("¿Realmente desea eliminar el registro?");
                    msj.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            UsuarioADO dbUsuario = new UsuarioADO(view.getContext());
                            if(dbUsuario.eliminar(id))
                                new Mensajes(view.getContext()).alert("Registro eliminado", "Se ha eliminado el registro correctamente.");
                            else
                                new Mensajes(view.getContext()).alert("Error", "Se ha producido un error al intentar eliminar el registro (SQLite).");

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            database.getReference().child("Usuario").child(String.valueOf(id)).removeValue();

                            ((AdminUsuariosActivity) view.getContext()).recreate();
                        }
                    });
                    msj.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    msj.create();
                    msj.show();
                }
            });

        }

        public void asignarRegistros(Usuario us)
        {
            txtNombreCompleto.setText(us.getNombres() + " " + us.getApellidos());
            txtEmail.setText(us.getEmail());
            id = us.getUid();
        }
    }
}
