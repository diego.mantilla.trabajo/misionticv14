package com.example.misionticv14.ado;

import com.example.misionticv14.modelos.Usuario;

import junit.framework.TestCase;

public class UsuarioADOTest extends TestCase {

    public void testValidarUsuario() {
        UsuarioADO us = new UsuarioADO(null);

        Usuario modelo = new Usuario();
        modelo.setEmail("user@gmail.com");
        modelo.setClave("root");

        boolean resultado = us.validarUsuario(modelo);

        assertEquals(true, resultado);
    }

    public void testTriangulo()
    {
        UsuarioADO us = new UsuarioADO(null);

        double base = 15;
        double altura = 4;
        double resultado = us.obtenerArea(base, altura);

        assertEquals(Double.parseDouble("30"), resultado);
    }
}