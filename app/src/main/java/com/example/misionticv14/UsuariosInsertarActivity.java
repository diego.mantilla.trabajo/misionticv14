package com.example.misionticv14;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class UsuariosInsertarActivity extends AppCompatActivity {

    private Usuario registro=null;
    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtEmail;
    private EditText txtClave;
    private EditText txtLatitud;
    private EditText txtLongitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios_insertar);
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        Button btnVolver = (Button) findViewById(R.id.usuarios_insertar_btnVolver);
        Button btnGuardar = (Button) findViewById(R.id.usuarios_insertar_btnGuardar);
        txtNombres = (EditText) findViewById(R.id.usuarios_insertar_txtNombres);
        txtApellidos = (EditText) findViewById(R.id.usuarios_insertar_txtApellidos);
        txtEmail = (EditText) findViewById(R.id.usuarios_insertar_txtEmail);
        txtClave = (EditText) findViewById(R.id.usuarios_insertar_txtClave);
        txtLatitud = (EditText) findViewById(R.id.usuarios_insertar_txtLatitud);
        txtLongitud = (EditText) findViewById(R.id.usuarios_insertar_txtLongitud);

        Bundle parametros = getIntent().getExtras();
        if(parametros!=null)
        if(parametros.containsKey("id"))
        {
            UsuarioADO db = new UsuarioADO(this);
            Usuario us = db.obtenerUsuario(parametros.getString("id"));
            if(us==null) {
                String id = parametros.getString("id");
                database.getReference().child("Usuario").child(id).get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if(task.isSuccessful()) {
                            registro = task.getResult().getValue(Usuario.class);
                            cargarDatos();
                            
                        }
                    }
                });
            }
            else {
                registro = us;
                cargarDatos();
            }
        }

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombres = txtNombres.getText().toString();
                String apellidos = txtApellidos.getText().toString();
                String email = txtEmail.getText().toString();
                String clave = txtClave.getText().toString();
                double latitud = 0;
                double longitud = 0;

                try {
                    latitud = Double.parseDouble(txtLatitud.getText().toString());
                    longitud = Double.parseDouble(txtLongitud.getText().toString());
                }
                catch (Exception ex) {
                    txtLatitud.setText("0");
                    txtLongitud.setText("0");
                }

                if(validarCampos(nombres, apellidos, email, clave, latitud, longitud))
                {
                    if(registro!=null)
                    {
                        registro.setNombres(nombres);
                        registro.setApellidos(apellidos);
                        registro.setEmail(email);
                        registro.setClave(clave);
                        registro.setLatitud(latitud);
                        registro.setLongitud(longitud);

                        UsuarioADO db = new UsuarioADO(view.getContext());
                        if(db.editar(registro))
                            new Mensajes(view.getContext()).alert("Registro actualizado", "Se ha actualizado el registro correctamente.");
                        else
                            new Mensajes(view.getContext()).alert("Error", "Se ha producido un error al intentar actualizar el registro.");
                    }
                    else {
                        UsuarioADO registroADO = new UsuarioADO(view.getContext());
                        registro = new Usuario();
                        registro.setNombres(nombres);
                        registro.setApellidos(apellidos);
                        registro.setEmail(email);
                        registro.setClave(clave);
                        registro.setLatitud(latitud);
                        registro.setLongitud(longitud);
                        registro.setUid(UUID.randomUUID().toString());
                        long idInsercion = registroADO.insertar(registro);
                        registro.setId((int) idInsercion);
                        if (idInsercion > 0)
                            new Mensajes(view.getContext()).alert("Registro insertado", "Se ha insertado el registro correctamente con el codigo " + String.valueOf(idInsercion));
                        else
                            new Mensajes(view.getContext()).alert("Error", "Se ha producido un error al intentar insertar el registro.");
                    }

                    // Write a message to the database
                    database.getReference().child("Usuario").child(registro.getUid()).setValue(registro);
                    FirebaseAuth autenticacion = FirebaseAuth.getInstance();
                    autenticacion.createUserWithEmailAndPassword(registro.getEmail(), registro.getClave());


                    onBackPressed();
                }
                else
                {
                    new Mensajes(view.getContext()).alert("Advertencia", "Digite los campos en blanco.");
                }
            }
        });
    }

    public static boolean validarCampos(String nombres, String apellidos, String email, String clave, double latitud, double longitud)
    {
        boolean camposAceptados = false;

        if((!nombres.isEmpty() && !apellidos.isEmpty() && !email.isEmpty() && !clave.isEmpty() && !(latitud==0) && !(longitud==0)))
            camposAceptados=true;

        return camposAceptados;
    }

    public void cargarDatos()
    {
        this.txtNombres.setText(this.registro.getNombres());
        this.txtApellidos.setText(this.registro.getApellidos());
        this.txtEmail.setText(this.registro.getEmail());
        this.txtClave.setText(this.registro.getClave());
    }
}