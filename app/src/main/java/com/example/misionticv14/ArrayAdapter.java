package com.example.misionticv14;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Usuario;

import java.util.ArrayList;

public class ArrayAdapter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_adapter);

        Spinner spSpinner = (Spinner) findViewById(R.id.arrayadapter_spinner);

        android.widget.ArrayAdapter<String> adaptador = new android.widget.ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item);

/*        UsuarioADO us = new UsuarioADO(this);
        ArrayList<Usuario> usuarios = us.listar();

        for (int i=0; i<usuarios.size();i++)
            adaptador.add(usuarios.get(i).getNombres() + " " + usuarios.get(i).getApellidos());
*/
        adaptador.add("Restaurante");
        adaptador.add("Cafeteria");

        spSpinner.setAdapter(adaptador);
        spSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                new Mensajes(view.getContext()).alert("Titulo", "Cuerpo del mensaje");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}