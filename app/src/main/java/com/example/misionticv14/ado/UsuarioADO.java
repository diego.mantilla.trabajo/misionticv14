package com.example.misionticv14.ado;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.misionticv14.clases.SqliteConex;
import com.example.misionticv14.modelos.Usuario;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class UsuarioADO extends SqliteConex {

    private Context contexto;

    public UsuarioADO(@Nullable Context c)
    {
        super(c);
        this.contexto = c;
    }

    public long insertar(Usuario us)
    {
        long id = 0;

        SqliteConex dbc = new SqliteConex(this.contexto);
        SQLiteDatabase db = dbc.getWritableDatabase();
        try
        {

            ContentValues valores = new ContentValues();
            valores.put("uid", us.getUid());
            valores.put("nombres", us.getNombres());
            valores.put("apellidos", us.getApellidos());
            valores.put("email", us.getEmail());
            valores.put("clave", us.getClave());
            valores.put("latitud", us.getLatitud());
            valores.put("longitud", us.getLongitud());

            id = db.insert("usuarios", null, valores);
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        finally {
            db.close();
        }

        return id;
    }

    public Usuario obtenerUsuario(String id)
    {
        Usuario us = null;

        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();

        try
        {
            Cursor cregistros = db.rawQuery("select id, uid, nombres, apellidos, email, clave, latitud, longitud from usuarios where uid = " + id, null);
            cregistros.moveToFirst();
            us = new Usuario();
            us.setId(cregistros.getInt(0));
            us.setUid(cregistros.getString(1));
            us.setNombres(cregistros.getString(2));
            us.setApellidos(cregistros.getString(3));
            us.setEmail(cregistros.getString(4));
            us.setClave(cregistros.getString(5));
            us.setLatitud(cregistros.getDouble(6));
            us.setLongitud(cregistros.getDouble(7));
        }
        catch (Exception ex)
        {

        }
        finally {
            db.close();
        }
        return us;
    }

    public ArrayList<Usuario> listar()
    {
        ArrayList<Usuario> registros = new ArrayList<>();
        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();

        try {
            Cursor cregistros = db.rawQuery("select id, nombres, apellidos, email, clave, latitud, longitud from usuarios", null);

            if (cregistros.moveToFirst())
                do {
                    Usuario us = new Usuario();
                    us.setId(cregistros.getInt(0));
                    us.setNombres(cregistros.getString(1));
                    us.setApellidos(cregistros.getString(2));
                    us.setEmail(cregistros.getString(3));
                    us.setClave(cregistros.getString(4));
                    us.setLatitud(cregistros.getDouble(5));
                    us.setLongitud(cregistros.getDouble(6));

                    registros.add(us);
                } while (cregistros.moveToNext());
        }
        catch (Exception ex)
        {

        }
        finally {
            db.close();
        }

        return registros;
    }

    public boolean editar(Usuario us)
    {
        boolean editado = false;

        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();
        try
        {

            db.execSQL("UPDATE usuarios" +
                    "   SET nombres = '" + us.getNombres() + "'," +
                    "       apellidos = '" + us.getApellidos() + "'," +
                    "       email = '" + us.getEmail() + "'," +
                    "       clave = '" + us.getClave() + "'" +
                    " WHERE uid = '" + String.valueOf(us.getUid()) + "'");
            editado=true;

        }
        catch (Exception ex)
        {

        }
        finally {
            db.close();
        }

        return editado;
    }

    public boolean eliminar(String id)
    {
        boolean eliminado = false;

        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();

        try
        {
            db.execSQL("DELETE FROM usuarios\n" +
                    "      WHERE uid = '" + String.valueOf(id) + "'");
            eliminado=true;
        }
        catch (Exception ex)
        {

        }
        finally {
            db.close();
        }

        return eliminado;
    }

    public boolean validarUsuario(Usuario us)
    {
        boolean validado = false;

        String emailFijo = "user@gmail.com";
        String claveFija = "root";

        if(us.getEmail().equals(emailFijo) && us.getClave().equals(claveFija))
            validado=true;

        return validado;
    }

    public double obtenerArea(double base, double altura)
    {
        double resultado = (base * altura) / 2;

        return resultado;
    }
}
