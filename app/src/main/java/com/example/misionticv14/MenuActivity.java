package com.example.misionticv14;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.misionticv14.clases.Mensajes;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_Misionticv14_barra);
        setContentView(R.layout.activity_menu);

        FloatingActionButton btnMas = (FloatingActionButton) findViewById(R.id.menu_btnMas);
        FloatingActionButton btnToast = (FloatingActionButton) findViewById(R.id.menu_toast);
        FloatingActionButton btnSnackbar = (FloatingActionButton) findViewById(R.id.menu_snackbar);
        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog dlg = new BottomSheetDialog(view.getContext());
                dlg.setContentView(R.layout.menubottom_ejemplo);
                dlg.show();
            }
        });

        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Mensaje mostrado desde Toast", Toast.LENGTH_LONG).show();
            }
        });

        btnSnackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Mensaje presentado desde el Snackbar", BaseTransientBottomBar.LENGTH_LONG).show();
            }
        });


        try {
            Bundle b = getIntent().getExtras();
            TextView txvUsuario = (TextView) findViewById(R.id.menu_txvUsuario);
            txvUsuario.setText(b.getString("usuario"));
        }
        catch (Exception ex)
        {

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater propiedadesMenu = getMenuInflater();
        propiedadesMenu.inflate(R.menu.menu_principal, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        Mensajes msj = new Mensajes(this);
        Intent i = null;

        switch (item.getItemId())
        {
            //Intent explicito, se comunica o carga un activity
            case R.id.principal_mniAcercaDe: i = new Intent(this, AcercaDeActivity.class);
                break;
            //Intent implicito, carga una pagina web
            case R.id.principal_mniPerfil: i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://es.wikipedia.org/wiki/Marco_van_Basten"));
                break;
            //Intent implicito, carga el menu de llamada
            case R.id.principal_mniPuntos: i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:310645789"));
                break;
            case R.id.principal_mniCerrarSesion:
                FirebaseAuth autenticacion = FirebaseAuth.getInstance();
                autenticacion.signOut();
                onBackPressed();
                break;
        }
        //Metodo que inicia o carga el intent
        if(i!=null)
        startActivity(i);

        return super.onOptionsItemSelected(item);
    }
}