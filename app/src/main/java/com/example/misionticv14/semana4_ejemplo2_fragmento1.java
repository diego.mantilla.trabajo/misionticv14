package com.example.misionticv14;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import com.example.misionticv14.adapters.AdapterLista;
import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.modelos.Usuario;
import com.example.misionticv14.viewmodels.UsuarioViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento1 extends Fragment {

    public semana4_ejemplo2_fragmento1() {
        // Required empty public constructor
    }

    public static semana4_ejemplo2_fragmento1 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento1 fragment = new semana4_ejemplo2_fragmento1();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento1, container, false);

        ListView lstLista = (ListView) vista.findViewById(R.id.semana4_ejemplo2_fragmento1_lstItems);
        UsuarioADO dbUsuario = new UsuarioADO(vista.getContext());
        ArrayList<Usuario> usuarios = dbUsuario.listar();

        AdapterLista adaptador = new AdapterLista(usuarios);

        lstLista.setAdapter(adaptador);

        lstLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Usuario us = new Usuario();

                TwoLineListItem vista = (TwoLineListItem) view;
                String id = vista.getContentDescription().toString();

                UsuarioADO usuariodb = new UsuarioADO(view.getContext());
                us = usuariodb.obtenerUsuario(id);

                UsuarioViewModel usuariovm = ViewModelProviders.of(getActivity()).get(UsuarioViewModel.class);
                usuariovm.setUsuario(us);

            }
        });

        return vista;
    }
}