package com.example.misionticv14.adapters;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.example.misionticv14.R;
import com.example.misionticv14.modelos.Usuario;

import java.util.ArrayList;

public class AdapterSpinner implements SpinnerAdapter {

    private final ArrayList<Usuario> datos;

    public AdapterSpinner(ArrayList<Usuario> datos) {
        this.datos = datos;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        TwoLineListItem tv = (TwoLineListItem) LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_expandable_list_item_2, viewGroup, false );
        tv.getText1().setText(datos.get(i).getNombres() + " " + datos.get(i).getApellidos());
        tv.getText2().setText(datos.get(i).getEmail());
        return tv;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int i) {
        return datos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView v = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.support_simple_spinner_dropdown_item, viewGroup, false);
        v.setText(datos.get(i).getNombres() + " " + datos.get(i).getApellidos());
        return v;
    }

    @Override
    public int getItemViewType(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
