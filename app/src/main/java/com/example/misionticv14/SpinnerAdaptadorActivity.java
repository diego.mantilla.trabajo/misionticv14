package com.example.misionticv14;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.misionticv14.adapters.AdapterSpinner;
import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Usuario;

import java.util.ArrayList;

public class SpinnerAdaptadorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_adaptador);

        Spinner spnLista = (Spinner) findViewById(R.id.spinner_adaptador);

        UsuarioADO db = new UsuarioADO(this);
        ArrayList<Usuario> usuarios = db.listar();

        AdapterSpinner adaptador = new AdapterSpinner(usuarios);
        spnLista.setAdapter(adaptador);

        spnLista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                new Mensajes(view.getContext()).alert("Titulo", "Seleccion");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}