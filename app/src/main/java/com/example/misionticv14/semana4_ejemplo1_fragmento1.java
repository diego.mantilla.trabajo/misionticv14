package com.example.misionticv14;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.misionticv14.clases.Mensajes;
import com.example.misionticv14.modelos.Operacion;
import com.example.misionticv14.viewmodels.ResultadoViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo1_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo1_fragmento1 extends Fragment {


    public semana4_ejemplo1_fragmento1() {
        // Required empty public constructor
    }

    public static semana4_ejemplo1_fragmento1 newInstance(String param1, String param2) {
        semana4_ejemplo1_fragmento1 fragment = new semana4_ejemplo1_fragmento1();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_semana4_ejemplo1_fragmento1, container, false);

        EditText txtNumero1 = (EditText) v.findViewById(R.id.ejemplo1_fragmento1_txtNumero1);
        EditText txtNumero2 = (EditText) v.findViewById(R.id.ejemplo1_fragmento1_txtNumero2);
        Button btnCalcular = (Button) v.findViewById(R.id.ejemplo1_fragmento1_btnCalcular);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num1 = Integer.parseInt(txtNumero1.getText().toString());
                int num2 = Integer.parseInt(txtNumero2.getText().toString());

                ResultadoViewModel resultadovm = ViewModelProviders.of(getActivity()).get(ResultadoViewModel.class);
                Operacion objOperacion = new Operacion();
                objOperacion.setNumero1(num1);
                objOperacion.setNumero2(num2);

                resultadovm.setOperacion(objOperacion);
            }
        });

        return v;
    }
}