package com.example.misionticv14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.misionticv14.adapters.AdapterRecyclerView;
import com.example.misionticv14.ado.UsuarioADO;
import com.example.misionticv14.modelos.Usuario;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminUsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_usuarios);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        try {
            database.setPersistenceEnabled(true);
        }
        catch (Exception ex)
        {}

   //     this.actualizarLista();

        // Read from the database
        RecyclerView rcvUsuarios = (RecyclerView) findViewById(R.id.admin_usuarios_rcvUsuarios);
        rcvUsuarios.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<Usuario> usuarios = new ArrayList<>();
        database.getReference().child("Usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                usuarios.clear();
                for(DataSnapshot nodoHijo : dataSnapshot.getChildren())
                {
                    Usuario us = nodoHijo.getValue(Usuario.class);
                    usuarios.add(us);
                }

                AdapterRecyclerView adaptador = new AdapterRecyclerView(usuarios);
                rcvUsuarios.setAdapter(adaptador);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });


        FloatingActionButton btnAgregarUsuario = (FloatingActionButton) findViewById(R.id.admin_usuarios_btnAgregar);

        btnAgregarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), UsuariosInsertarActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        actualizarLista();
    }

    private void actualizarLista()
    {
        RecyclerView rcvUsuarios = (RecyclerView) findViewById(R.id.admin_usuarios_rcvUsuarios);
        rcvUsuarios.setLayoutManager(new LinearLayoutManager(this));

        UsuarioADO db = new UsuarioADO(this);
        ArrayList<Usuario> usuarios = db.listar();

        AdapterRecyclerView adaptador = new AdapterRecyclerView(usuarios);
        rcvUsuarios.setAdapter(adaptador);

    }
}